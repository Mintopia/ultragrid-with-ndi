FROM ubuntu:18.04 AS builder
MAINTAINER Jessica Smith <jess@mintopia.net>

ENV DEBIAN_FRONTEND noninteractive

# This copies in the NDI SDK library and a couple of things
COPY overlay /

# Get our dependencies
RUN \
	apt-get -y update && \
	apt-get -y install \
		build-essential \
		git \
		autoconf \
		automake \
		pkg-config \
		libtool \
		libopencv-dev \
		libsdl2-dev \
		libglib2.0-dev \
		libcurl4-nss-dev \
		liblivemedia-dev \
		libglew-dev \
		freeglut3-dev \
		libgl1-mesa-dev \
		libx11-dev \
		nvidia-cuda-toolkit \
		portaudio19-dev \
		libjack-jackd2-dev \
		libasound-dev \
		libv4l-dev \
		libavcodec-dev \
		libavutil-dev \
		libssl-dev \
		uuid-dev \ 
		yasm \
		libopus-dev \
		libvpx-dev \
		libx264-dev \
		nasm \
		libx265-dev \
		libnuma-dev \
		libvpx-dev \
		libfdk-aac-dev \
		libmp3lame-dev \
		cmake \
		zip

# Build ffmpeg
RUN \
	git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git && \
	cd nv-codec-headers && \
	make && \
	make install && \
	cd / && \
	git clone git://source.ffmpeg.org/ffmpeg.git && \
	cd ffmpeg && \
	./configure --enable-gpl --enable-libx264 --enable-libopus --enable-libx265 --enable-nonfree --enable-nvenc && \
	make && \
	make install

# Now our UltraGrid submodule dependencies
RUN \
	git clone https://github.com/CESNET/UltraGrid.git && \
	cd UltraGrid && \
	git submodule update --init && \
	cd gpujpeg && \
	./autogen.sh && \
	make && \
	make install && \
	cd ../cineform-sdk && \
	cmake . && \
	make CFHDCodecStatic && \
	cd .. && \
	ldconfig

# Build UltraGrid
RUN \
	cd UltraGrid && \
	./autogen.sh && \
	make && \
	make install && \
	uv -v

# Prepare our static libraries for the next stage
RUN \
	chmod +x /deps.sh && \
	mkdir /libs && \
	/bin/bash deps.sh -b /usr/local/bin/uv -t /libs

FROM ubuntu:18.04
MAINTAINER Jessica Smith <jess@mintopia.net>

# Copy the artifacts from the build stage
COPY --from=builder /libs /
COPY --from=builder /usr/local/bin/uv /usr/local/bin/uv

# Update our libraries and run uv -v for sanity
RUN \
	ldconfig && \
	uv -v

ENTRYPOINT ["uv"]