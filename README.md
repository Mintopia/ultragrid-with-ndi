# UltraGrid with NDI

## Introduction

This is a build of UltraGrid 1.5+ from source with support for NDI.

## Usage

For main usage of UltraGrid see the [UltraGrid documentation](https://github.com/CESNET/UltraGrid/wiki).

For example, to view a list of NDI sources:

```
docker run --network=host uv:latest -t ndi:help 
```

Everything after `uv:latest` will be passed on to UltraGrid. Host networking is used to keep it simple, but you can specify the exact ports if you choose and use another networking type.

## Building

Building this, and all dependencies, is not a quick process. It requires ffmpeg, opencv, many, MANY, libraries, some of which require agreeing to EULA.

## Licenses

You don't want to know. This is probably not licensed suitably for any purpose. However, see the licenses directory for relevant licenses. This should not be distributed in current format without an army of lawyers providing legal advice for exactly what notices should be included.